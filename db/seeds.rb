# create 1 guest without household
puts "Creating 'guests without household'"

1.times do
  Guest.create(name: Faker::Name.name,
              household_id: 1000,
              plusone: false,
              completed: false)
end


# create 5  guests with household
puts "Creating 'guests with household'"
2.times do
  Guest.create(name: Faker::Name.name,
              plusone: false,
              household_id: 1003,
              completed: false)
end


puts "Creating 'guests with household and email'"
3.times do
  Guest.create(name: Faker::Name.name,
              plusone: false,
              household_id: 1004,
              email: "example@rsvp.com",
              completed: false)
end

