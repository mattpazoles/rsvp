class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :name
      t.boolean :attending
      t.text :notes
      t.boolean :plusone
      t.boolean :plusone_attending
      t.text :plusone_notes
      t.integer :household_id
      t.string :email
      t.boolean :completed

      t.timestamps null: false
    end
    add_index :guests, [:id, :name, :household_id]

  end
end
