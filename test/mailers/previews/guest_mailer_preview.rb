# Preview all emails at http://localhost:3000/rails/mailers/guest_mailer
class GuestMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/guest_mailer/rsvp_confirmation
  def rsvp_confirmation
    guest = Guest.first
    
    GuestMailer.rsvp_confirmation(guest)
  end

end
