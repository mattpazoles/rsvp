require 'test_helper'

class GuestMailerTest < ActionMailer::TestCase
  test "rsvp_confirmation" do
    guest = Guest.first
    
    mail = GuestMailer.rsvp_confirmation(guest)
    
    assert_equal "Wedding RSVP Confirmation", mail.subject
    assert_equal guest.email, mail.to
    assert_equal ["pazoleswedding@gmail.com"], mail.from
    assert_match "PazolesWedding", mail.body.encoded
  end

end
