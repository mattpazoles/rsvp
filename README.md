RSVP Rails App


Populate database using the db/seeds.rb file. Use faker gem to create dummy data while testing.



Uses a Redis DB/Soulmate gem to power the autocomplete form on the home page, guests model has a function to load_into_soulmate and remove_from_soulmate. Typing into the text field will return name matches, which are hyperlinked to the relevant form.


Each guest has fields for notes, if they're attending, their email, a "household ID", whether they've completed their RSVP form yet, and relevant information for a plus one if they have one. Use household ID to group people together so that one person can RSVP for everyone else in the household and their plus ones. 



Connects to Google Sheets to update a tracker to log all the information from the form. Uses Sucker Punch gem/delayed jobs to do this asynchronously. You'll need to grant permissions for the app on your google account if you'd like to use this feature (https://myaccount.google.com/permissions).



When deployed with SendGrid on Heroku, has mailers to send confirmation receipts via email.



Will only allow people to use the RSVP form once; app includes an error page instructing them to get in touch if they need to change their answers.