class GuestsController < ApplicationController
  before_action :set_guest, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, only: [:edit, :update]
  def index
    @guests = Guest.all
  end

  def search

  end

  def show
  end

  def new
    @guest = Guest.new
  end

  def edit
    @household_id = @guest.household_id
    @guests = Guest.where("household_id = ? AND NOT id = ?", @household_id, @guest)
    @groupcount = @guests.count+1
    @feed = @guest.feed

    if @guest.completed?
      redirect_to "/already_completed"
    end
  end

  def update
    @guest = Guest.find(params[:id])
    @guestorig = Guest.find(params[:id])
    @household_id = @guest.household_id
    @guests = Guest.where("household_id = ? AND NOT id = ?", @household_id, @guest)
    @groupcount = @guests.count+1
    
    if @groupcount == 1
      @guest.update_attributes(guest_params)
      UpdateDrive.new.async.perform(@guest)
      if @guest.completed?
        redirect_to "/success"
        GuestMailer.rsvp_confirmation(@guest).deliver_now
      else
        redirect_to "/error"
      end
    else
      Guest.update(params[:guests].keys, params[:guests].values)
      UpdateDrive.new.async.perform(@guestorig)
      params[:guests].keys.each do |id|
        @guest = Guest.find(id.to_i)
        UpdateDrive.new.async.perform(@guest)
      end
      @guestorig.update_attributes(guest_params)
      if @guestorig.completed?
        redirect_to "/success"
        GuestMailer.rsvp_confirmation(@guestorig).deliver_now
      else
        redirect_to "/error"
      end
    end

  end

  def create
    @guest = Guest.new(guest_params)

    respond_to do |format|
      if @guest.save
        format.html { redirect_to @guest, notice: 'Guest was successfully created.' }
        format.json { render :show, status: :created, location: @guest }
      else
        format.html { render :new }
        format.json { render json: @guest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guests/1
  # DELETE /guests/1.json
  def destroy
    @guest.destroy
    respond_to do |format|
      format.html { redirect_to guests_url, notice: 'Guest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_all
    params['guest'].keys.each do |id|
      @guest = Guest.find(id.to_i)
      @guest.update_attributes(guest_params(id))
    end
    redirect_to(guests_url)
  end

  def edit_all
    @guests = Guest.all
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_guest
    if params[:search]
      name = params[:search]
      @guest = Guest.find_by('lower(name) LIKE lower(?)',  "%#{name}%")
    else
      @guest = Guest.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def guest_params
    params.require(:guest).permit(:name, :attending, :notes, :plusone, :plusone_attending, :plusone_notes, :completed, :email)
  end

end
