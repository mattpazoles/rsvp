require "rubygems"
require "google/api_client"
require "google_drive"

class Guest < ActiveRecord::Base
  after_save :load_into_soulmate
  before_destroy :remove_from_soulmate


  # It returns the articles whose titles contain one or more words that form the query
  def self.search(query)
    # where(:title, query) -> This would return an exact match of the query
    where("lower(name) like lower(?)", "%#{query}%").first
  end
    
  def send_activation_email
    GuestMailer.rsvp_confirmation(self).deliver_now
  end  
    
  def group
    Guest.where("household_id = ?", household_id)
  end

  def plusone?
    return false if plusone.nil?
  end

  def feed
    Guest.where("household_id = ?", @household_id)
  end
  
  
  
  def update_spreadsheet
    access_token = ::Token.last.fresh_token
    connection = ::GoogleDrive.login_with_oauth(access_token)
    ss = connection.spreadsheet_by_title('RSVPs')
    if ss.nil?
      ss = connection.create_spreadsheet('RSVPs')
    end
    ws = ss.worksheets[0]
    last_row = 1 + ws.num_rows
    ws.update_cells(last_row, 1, [[id,name,attending,notes,plusone,self.plusone_attending,
                       plusone_notes,email,household_id,updated_at]])
    ws.save
  end
  

  private

  def load_into_soulmate
    loader = Soulmate::Loader.new("guests")
    loader.add("term" => name,
    "id" => self.id,
    "data" => {
      "link" => Rails.application.routes.url_helpers.edit_guest_path(self)
    })
  end

  def remove_from_soulmate
    loader = Soulmate::Loader.new("guests")
    loader.remove("id" => self.id)
  end
end
