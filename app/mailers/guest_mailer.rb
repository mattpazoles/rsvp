class GuestMailer < ApplicationMailer
  default from: "pazoleswedding@gmail.com"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.guest_mailer.rsvp_confirmation.subject
  #
  def rsvp_confirmation(guest)
    @guest = guest
    @email = guest.email
    @household_id = guest.household_id
    @updated = @guest.updated_at - 1.minute
    @guests = Guest.where("household_id = ? AND updated_at > ?", @household_id, @updated)
    mail(to: guest.email, subject: "Wedding RSVP Confirmation")
  end
end
