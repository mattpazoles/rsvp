Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV['client_id'], ENV['client_secret'], {
  scope: ['email', 'https://www.googleapis.com/auth/drive.file'],
    access_type: 'offline'}

end